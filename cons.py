import subprocess

# Running the Windows command (for example: 'dir' to list directory contents)
cmd = "dir"  # Replace this with any command you want to run

# Use subprocess to run the command
result = subprocess.run(cmd, shell=True, capture_output=True, text=True)

# Capture and print the output
print("Command Output:")
print(result.stdout)

# Capture and print any error
if result.stderr:
    print("Error:")
    print(result.stderr)
