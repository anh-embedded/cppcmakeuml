#ifndef libs_osvPlc_inc_IObservable_hpp
#define libs_osvPlc_inc_IObservable_hpp
#include "IObserver.cpp"
#include <string>

namespace osvPlc
{
    class IObservable
    {
    public:
        IObservable() = default;
        virtual ~IObservable() = default;

    virtual void registerObserver(IObserver* o) = 0;
    virtual void removeObserver(IObserver* o) = 0;
    virtual void notifyObservers(std::string_view const & filedID) = 0;
    };

}

#endif /* libs_osvPlc_inc_IObservable_hpp */
