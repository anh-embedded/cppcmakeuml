#ifndef libs_osvPlc_inc_IObserver_hpp
#define libs_osvPlc_inc_IObserver_hpp

#include <string>
#include "signalDef.hpp"

namespace osvPlc
{

    class IObserver
    {
      virtual void update() = 0;
      virtual ~IObserver() = default;
    };

};

#endif /* libs_osvPlc_inc_IObserver_hpp */
