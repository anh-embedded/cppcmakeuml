#ifndef libs_osvPlc_inc_signalDef_hpp
#define libs_osvPlc_inc_signalDef_hpp


namespace osvPlc
{
    enum class SIGNAL {
        SET_POINT,
        FEED_BACK,
        CURRENT
    };
};



#endif /* libs_osvPlc_inc_signalDe_hpp */
