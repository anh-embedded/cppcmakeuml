#ifndef libs_osvPlc_inc_motor_hpp
#define libs_osvPlc_inc_motor_hpp

#include "IObservable.hpp"

namespace osvPlc
{
    class motor : public IObservable
    {
        void registerObserver(IObserver* o) override;
        void removeObserver(IObserver* o) override;
        void notifyObservers(std::string_view const & filedID) override;
    };
};


#endif /* libs_osvPlc_inc_motor_hpp */
