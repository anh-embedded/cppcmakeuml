#include "database.hpp"
#include <algorithm>

void Database::registerObserver(IObserver* o) { mObservers.push_back(o); };

void Database::removeObserver(IObserver* o) {
  auto observer = std::find(mObservers.begin(), mObservers.end(), o);
  if (observer != mObservers.end()) {
    mObservers.erase(observer,
                     observer + 1);
  }
};

void Database::notifyObservers() {
  for (auto& o : mObservers) {
    o->update(mOperation, mRecord);
  }
};

void Database::editRecord(std::string operation, std::string record) {
  mOperation = operation;
  mRecord = record;
  notifyObservers();
};
