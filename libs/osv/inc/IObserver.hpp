#ifndef libs_osv_inc_IObserver_hpp
#define libs_osv_inc_IObserver_hpp
#include <string>

class IObserver
{
public:
    virtual void update(std::string operation, std::string record) = 0;
};
class ISubject
{
public:
    virtual void registerObserver(IObserver* o) = 0;
    virtual void removeObserver(IObserver* o) = 0;
    virtual void notifyObservers() = 0;
};

#endif /* libs_osv_inc_IObserver_hpp */
