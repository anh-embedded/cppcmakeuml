#ifndef libs_osv_inc_user_hpp
#define libs_osv_inc_user_hpp
#include <iostream>
#include <string>

#include "IObserver.hpp"

using namespace std;

class Client : public IObserver {
 public:
  Client() = default;
  void update(string operation, string record) final;
};

class Developer : public IObserver {
 public:
  Developer()  = default;
  void update(string operation, string record) final;
};

class Boss : public IObserver {
 public:
  Boss()  = default;
  void update(string operation, string record) final;
};

#endif /* libs_osv_inc_user_hpp */
