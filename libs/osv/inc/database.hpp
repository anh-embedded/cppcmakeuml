#ifndef libs_osv_inc_database_hpp
#define libs_osv_inc_database_hpp
#include <string>
#include <vector>

#include "IObserver.hpp"

class Database : public ISubject {
 private:
  std::vector<IObserver*> mObservers;
  std::string mOperation;
  std::string mRecord;

 public:
  Database() {}
  void registerObserver(IObserver* o) override ;
  void removeObserver(IObserver* o) override ;
  void notifyObservers() override ;
  void editRecord(std::string operation, std::string record);
};

#endif /* libs_osv_inc_database_hpp */
