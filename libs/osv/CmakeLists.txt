set(MODULE_NAME "osv")
message (STATUS "ADD SUB DIR : < ${MODULE_NAME} >")

file(GLOB_RECURSE MODULE_FILES src/*.c src/*.cpp inc/*)

if(MODULE_FILES)
    message(STATUS "ADD STATIC LIB: < ${MODULE_NAME} >")
    add_library (${MODULE_NAME} STATIC ${MODULE_FILES})
    target_include_directories(${MODULE_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/inc)
endif()