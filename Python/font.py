fonts = {
    "title": ("Arial", 24, "bold"),
    "subtitle": ("Arial", 18, "italic"),
    "body": ("Arial", 12, "normal"),
    "caption": ("Arial", 10, "italic"),
    "button": ("Arial", 14, "bold"),
    "header": ("Times New Roman", 22, "bold"),
    "footer": ("Times New Roman", 10, "normal"),
    "quote": ("Georgia", 16, "italic"),
    "code": ("Courier New", 12, "normal"),
    "warning": ("Verdana", 14, "bold italic")
}