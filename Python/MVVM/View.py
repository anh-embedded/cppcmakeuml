from viewmodel import ViewModel
from tkinter import  Radiobutton, Tk
from tkinter import ttk ,Label, Button, Entry, IntVar, Frame
from ViewTabConfig import tab_config_T

# Create the main window
root = Tk()

root.title("Tabbed Widget Example")
root.geometry("600x400")
# Create the notebook (tabbed widget)
notebook = ttk.Notebook(root)
notebook.pack(side="left",
               fill="both",
               expand=True)

# Create tabs as frames
tab_config = tab_config_T(notebook)
tab_dhc = Frame(notebook)

notebook.add(tab_config, text="Tab config")
notebook.add(tab_dhc, text="Tab DHC")

# Add content to the tabs
label1 = Label(tab_config, text="This is Tab config")
label1.pack(pady=20)
label2 = Label(tab_dhc, text="This is Tab DHC")
label2.pack(pady=20)

root.mainloop()

