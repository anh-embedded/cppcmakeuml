class Model:
    def __init__(self):
        self.sum = 0

    def sumup(self, first_digit, second_digit):
        self.sum = first_digit + second_digit

    def get_sum(self):
        return self.sum