from viewmodel import ViewModel
from tkinter import  Radiobutton, Tk
from tkinter import ttk ,Label, Button, Entry, IntVar, Frame

class tab_config_T (Frame):
    def __init__(self, container):
        super().__init__(master= container) # Initialize the main window
        self.radio_var = IntVar()
        self.radio_var.set(1)  # Set default value

        options = ["Option 1", "Option 2", "Option 3"]
        for idx, option in enumerate(options, start=1):
            radio_button = Radiobutton(self, text=option, variable=self.radio_var, value=idx)
            radio_button.pack(side="left", padx=10, pady=10, fill="both", expand=True)
