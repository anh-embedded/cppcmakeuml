import tkinter as tk
from model import Model

class ViewModel:
    def __init__(self):
        self.model = Model()
        self.first_digit = tk.IntVar()
        self.second_digit = tk.IntVar()
        self.sum = tk.IntVar()

    def sumup(self):
        first_digit = self.first_digit.get()
        second_digit = self.second_digit.get()
        self.model.sumup(first_digit, second_digit)
        self.update_sum()
        self.first_digit.set("")
        self.second_digit.set("")

    def update_sum(self):
        self.sum.set(self.model.get_sum())