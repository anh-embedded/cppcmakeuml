import datetime

class Logger_T:
    def __init__(self, log_file='app.log'):
        self.log_file = log_file

    def log(self, message, level="INFO"):
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        formatted_message = f"{timestamp} - {level} - {message}"
        self._write_to_file(formatted_message)
        self._write_to_console(formatted_message)

    def log_i(self, message):
        self.log(message, level="INFO")

    def log_d(self, message):
        self.log(message, level="DEBUG")

    def log_v(self, message):
        self.log(message, level="VERBOSE")

    def _write_to_file(self, message):
        with open(self.log_file, 'a') as file:
            file.write(message + '\n')

    def _write_to_console(self, message):
        print(message)

# Example usage
if __name__ == "__main__":
    logger = Logger_T()
    logger.log_i("This is an info message.")
    logger.log_d("This is a debug message.")
    logger.log_v("This is a verbose message.")