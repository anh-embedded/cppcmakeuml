from tkinter import Tk
from tkinter.ttk import Notebook
from .View.View import View_T
from queue import Queue
class UI_T:
    def __init__(self, queue_to_logic_thread : Queue):
        self.root = Tk()
        self.root.title("LazyApp")
        self.root.geometry("800x400")
        self.View = View_T(self.root)
    def run(self):
        self.root.mainloop()

