import utility.utility as util
from utility.log import Logger_T
from threading import Thread
from UI.UI import UI_T
import queue

log = Logger_T()
q = queue.Queue()

def run_UI():
    UI = UI_T(q)   # Create an instance of the UI
    UI.run()   # Run the UI

def run_logic():
    pass

if __name__ == "__main__":
    Thread_UI = Thread(target=run_UI)   # Create a thread for the UI
    Thread_Logic = Thread(target=run_logic)   # Create a thread for the logic layer

    Thread_UI.start()   # Start the UI thread
    Thread_Logic.start()   # Start the logic thread

    Thread_UI.join()   # Wait for the UI thread to finish
    Thread_Logic.join()   # Wait for the logic thread to finish
    # Create a queue for communication between threads
    # Add a sentinel value to signal the end of the queue
    q.put(None)