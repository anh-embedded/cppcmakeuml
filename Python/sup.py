import tkinter as tk
from tkinter import ttk

class Model:
    """
    The Model class represents the data and business logic of the application.
    In this case, it simply stores the selected radio button value.
    """
    def __init__(self):
        self._selected_option = tk.StringVar(value="Option 1")

    @property
    def selected_option(self):
        return self._selected_option.get()

    @selected_option.setter
    def selected_option(self, value):
        self._selected_option.set(value)


class ViewModel:
    """
    The ViewModel acts as an intermediary between the Model and the View.
    It exposes the Model's data in a way that the View can bind to and
    handles user interactions by updating the Model.
    """
    def __init__(self, model):
        self.model = model

    def get_selected_option(self):
        return self.model.selected_option

    def set_selected_option(self, value):
        self.model.selected_option = value


class View(tk.Frame):
    """
    The View represents the user interface. It displays data from the ViewModel
    and captures user input.
    """
    def __init__(self, master, viewmodel):
        super().__init__(master)
        self.viewmodel = viewmodel
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        # Create radio buttons
        ttk.Radiobutton(self, text="Option 1", variable=self.viewmodel.model._selected_option,
                        value="Option 1", command=lambda: self.viewmodel.set_selected_option("Option 1")).pack(anchor=tk.W)
        ttk.Radiobutton(self, text="Option 2", variable=self.viewmodel.model._selected_option,
                        value="Option 2", command=lambda: self.viewmodel.set_selected_option("Option 2")).pack(anchor=tk.W)
        ttk.Radiobutton(self, text="Option 3", variable=self.viewmodel.model._selected_option,
                        value="Option 3", command=lambda: self.viewmodel.set_selected_option("Option 3")).pack(anchor=tk.W)

        # Create a label to display the selected option
        ttk.Label(self, textvariable=self.viewmodel.model._selected_option).pack()


if __name__ == "__main__":
    root = tk.Tk()
    model = Model()
    viewmodel = ViewModel(model)
    view = View(root, viewmodel)
    root.mainloop()