set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_COMPILER "C:/msys64/ucrt64/bin/g++.exe") #clang
set(CMAKE_C_COMPILER "C:/msys64/ucrt64/bin/gcc.exe") #clang++

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


set(COMPILER_WARNINGS_OPTIONS  -O0 -g -Wall -Wpedantic -Wconversion -Wsign-conversion -Wunreachable-code -Wattributes -fpermissive)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
    set(COMPILER_WARNINGS_OPTIONS ${COMPILER_WARNINGS_OPTIONS} -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic)
endif()

if(NOT APP_EXECUTABLE_NAME)
    set(APP_EXECUTABLE_NAME "appDefaultName")
endif()


set(LIBS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/libs")
set(APP_DIR "${CMAKE_CURRENT_SOURCE_DIR}/app")


