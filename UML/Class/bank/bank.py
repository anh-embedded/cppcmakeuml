class Person:
    def __init__(self, name: str, money: float):
        self.name = name  # Public attribute
        self.money = money  # Public attribute

    def __repr__(self):
        return f"Person(name={self.name}, money={self.money:.2f})"


class Bank:
    def __init__(self):
        self._accounts = []  # Private dynamic list of Person objects

    # Add a customer (pass by reference concept is handled by passing the object directly)
    def add(self, customer: Person):
        self._accounts.append(customer)

    # Remove a customer by name
    def remove(self, name: str):
        self._accounts = [customer for customer in self._accounts if customer.name != name]

    # Deposit money into a customer's account
    def deposit(self, name: str, money: float):
        for customer in self._accounts:
            if customer.name == name:
                customer.money += money
                print(f"{money:.2f} deposited to {name}'s account.")
                return
        print(f"Customer {name} not found.")

    # Withdraw money from a customer's account
    def withdraw(self, name: str, money: float):
        for customer in self._accounts:
            if customer.name == name:
                if customer.money >= money:
                    customer.money -= money
                    print(f"{money:.2f} withdrawn from {name}'s account.")
                else:
                    print(f"Insufficient funds for {name}.")
                return
        print(f"Customer {name} not found.")

    # Print all customers and their account balances
    def printAll(self):
        if self._accounts:
            print("Bank Accounts:")
            for customer in self._accounts:
                print(f"Name: {customer.name}, Money: {customer.money:.2f}")
        else:
            print("No accounts in the bank.")


# Example usage
if __name__ == "__main__":
    # Create the bank
    bank = Bank()

    # Create some people
    alice = Person("Alice", 1000.0)
    bob = Person("Bob", 500.0)

    # Add customers to the bank
    bank.add(alice)
    bank.add(bob)

    # Print all accounts
    bank.printAll()

    # Deposit money
    bank.deposit("Alice", 200)
    bank.printAll()

    # Withdraw money
    bank.withdraw("Bob", 100)
    bank.printAll()

    # Remove a customer
    bank.remove("Alice")
    bank.printAll()
